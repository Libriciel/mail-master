FROM centos:8
#Fork : invokr/mail
MAINTAINER Pierre-Emmanuel VIVER <p.viver@libriciel.coop>
MAINTAINER Rémi DUBOURGET <r.dubourget@libriciel.coop>

# Install postfix, dovecot, and supervisor
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm \
 && yum update -y && yum upgrade -y && yum install -y cronie cyrus-sasl dovecot opendkim \
    opendmarc postfix python3-setuptools pypolicyd-spf rsyslog wget supervisor python3-pip \
 && pip3 install mako dumb-init && yum clean all

# Environment variables
ENV POSTFIX_HOSTNAME="mail.libriciel.net"

# Add scripts and config
ADD scripts /opt/bin
ADD config /opt/config

# Add group and user for virtual mail
RUN groupadd -g 10000 vmail && useradd -m -d /vmail -u 10000 -g 10000 -s /sbin/nologin vmail && chmod 755 /vmail \
 && usermod -G vmail postfix && usermod -G vmail dovecot

# Add secure directory
RUN mkdir -p /secure/postfix && touch /secure/postfix/vmaps /secure/postfix/vhosts /secure/postfix/vuids /secure/postfix/vgids \
 && mkdir -p /secure/dovecot && touch /secure/dovecot/users /secure/dovecot/passwd

# Configure supervisord
RUN sed -i -e"s/\/run\/supervisor\/supervisor.sock/\/dev\/shm\/supervisor.sock/" /etc/supervisord.conf
ADD config/supervisor/supervisord.conf /etc/supervisord.d/mail.ini

# Configure rsyslogd
ADD config/rsyslog/file.conf /etc/rsyslog.conf

# Configure opendmarc
ADD config/opendmarc/opendmarc.conf /etc/opendmarc.conf
RUN ln -s /opt/bin/update-tld-names /etc/cron.weekly/ && /opt/bin/update-tld-names

# Configure postfix
ADD config/postfix /etc/postfix/

# Configure dovecot
ADD config/dovecot /etc/dovecot

# Expose smtpd, submission and imaps
EXPOSE 25
EXPOSE 587
EXPOSE 993

# Start our init system
CMD ["/usr/local/bin/dumb-init", "/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
